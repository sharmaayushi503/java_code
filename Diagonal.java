class Diagonal
{
static int MAX = 100 ;
static void printFirstDiagonal ( int mat [] [] , int n)
{
System.out.print (" First Diagonal : " ) ;
for ( int i = 0 ; i<n ; i++)
{
for ( int j = 0 ; j<n ; j++)
{
if (i == j)
{
System.out.print ( mat [i] [j] + " ," ) ;
}
}
}
System.out.println(" ") ;
}
static void printSecondDiagonal ( int mat [] [] , int n )
{
System.out.print (" second Diagonal : ") ;
for (int i = 0; i<n; i++)
{
for (int j = 0 ; j<n ; j++)
{
if  ( ( i+j) == (n - 1) )
{
System.out.print ( mat [i] [j] + " ,") ;
}
}
}
System.out.println(" ") ;
}
public static void main (String[] args)
{
int n = 4 ;
int a [] [] = {{1,2,3,4}, {5,6,7,8},{1,2,3,4},{5,6,7,8}} ;
printFirstDiagonal (a, n) ;
printSecondDiagonal (a, n) ;
}
}